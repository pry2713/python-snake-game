# README #

Snake Game [Using Python]

## Repository Info ##

* Easy Snake Game Development using Python 
* Use Python module ### pygame for making game

## Set up ##
### Installing Python 3 
```
install python3
```
### Installing pygame
```
pip install pygame
```
For Testing, run code directaly from .exe file on windows os
```
snakeGame.exe
```
### usefull links ###

[Python] (https://docs.python.org/3/)
[pygame] (https://www.pygame.org/docs/)

Happy Gaming :blush:

