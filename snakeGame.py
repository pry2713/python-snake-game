import pygame
import random
import os
import time

# Game Sound
pygame.mixer.init()

# colors
white = (255, 255, 255)
red = (255, 0, 0)
black = (0, 0, 0)
orange = (255, 165, 0)
green = (0,128,0)

# Creating Window
x = pygame.init()
width = 769
height = 556
gameWindow = pygame.display.set_mode((width, height))
pygame.display.set_caption("Snake Game")
clock = pygame.time.Clock()
# font = pygame.font.SysFont(None, 35)
font_path = pygame.font.match_font('edwardianscriptitc', bold=True)
font = pygame.font.Font(font_path, 55)
# font1 = pygame.font.get_fonts()
# print(font1)

# BackGround Image
path = os.getcwd()
bgImg = pygame.image.load(os.path.join(path, "imgs\snake1.png"))
gameOverImage = pygame.image.load(os.path.join(path,"imgs\game_over3.png"))
gameBgImage = pygame.image.load(os.path.join(path,"imgs\game_bg.png"))
bgImg = pygame.transform.scale(bgImg, (width, height)).convert_alpha()
gameOverImage = pygame.transform.scale(gameOverImage, (width, height)).convert_alpha()
gameBgImage = pygame.transform.scale(gameBgImage, (width, height)).convert_alpha()


def text_screen(text, color, x, y):
    screen_text = font.render(text, True, color)
    gameWindow.blit(screen_text, [x, y])

def plot_snake(gameWindow, color, snake_lst, snake_size, head):
    for x,y in snake_lst:
        pygame.draw.rect(gameWindow, color, [x, y, snake_size, snake_size])

def welcome():
    exit_game = False
    font_path = pygame.font.match_font('edwardianscriptitc', bold=True)
    font = pygame.font.Font(font_path, 75)
    while not exit_game:
        gameWindow.fill(orange)
        gameWindow.blit(bgImg, (0, 0))
        # text_screen("Welcome To", green, int(320), int(150))
        text_screen("Press Enter To Exit", red, int(170), int(380))
        for event in pygame.event.get():
            # print(event)
            if event.type == pygame.QUIT:
                exit_game = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    pygame.mixer.music.pause()
                    game_loop()
                if event.key == pygame.K_RETURN:
                    pygame.quit()
                    quit()
        pygame.display.update()
        clock.tick(30)
        
# Creating Game Loop
def game_loop():
    # Game specific Variable
    exit_game = False
    game_over = False
    snake_x = 45
    snake_y = 55
    food_x = random.randint(50, int(width-20))
    food_y = random.randint(50, int(height-20))
    init_vel = 5
    velocity_x = 0
    velocity_y = 0
    snake_size = 15
    score = 0
    fps = 30
    snk_length = 1
    snk_lst = []
    highscore = 0
    try:
        with open('highScore.txt', 'r') as f:
            highscore = f.read()
    except:
        with open('highScore.txt', 'w') as f:
            f.write("0")
    while not exit_game:
        if game_over:
            # pygame.mixer.music.load(os.path.join(path, 'imgs\game.mp3'))
            # pygame.mixer.music.play()
            # pygame.mixer.music.unpause()
            with open('highScore.txt', 'w') as f:
                f.write(str(highscore))
            gameWindow.fill(white)
            gameWindow.blit(gameOverImage, (0, 0))
            text_screen("Score: "+str(score), black, int(350), int(210))
            text_screen("Game Over! Press Enter To Continue", black, int(60), int(370))
            for event in pygame.event.get():
                # print(event)
                if event.type == pygame.QUIT:
                    exit_game = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        pygame.mixer.music.load(os.path.join(path, 'imgs\game.mp3'))
                        pygame.mixer.music.play(-1)
                        welcome()
        else:
            for event in pygame.event.get():
                # print(event)
                if event.type == pygame.QUIT:
                    exit_game = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        velocity_x = init_vel
                        velocity_y = 0
                    if event.key == pygame.K_LEFT:
                        velocity_x = -init_vel
                        velocity_y = 0
                    if event.key == pygame.K_UP:
                        velocity_y = -init_vel
                        velocity_x = 0
                    if event.key == pygame.K_DOWN:
                        velocity_y = init_vel
                        velocity_x = 0
                    # CHEAT CODE
                    if event.key == pygame.K_q:
                        score += 10
                        
            snake_x += velocity_x
            snake_y += velocity_y
            
            if abs(snake_x - food_x) < 10 and abs(snake_y - food_y) < 10:
                pygame.mixer.music.load(os.path.join(path, 'imgs\eat.mp3'))
                pygame.mixer.music.play()
                food_x = random.randint(50, int(width-50))
                food_y = random.randint(50, int(height-50))
                # init_vel += 1
                score += 10
                snk_length += 5
                if score > int(highscore):
                    highscore = score

            if snake_x < 0 or snake_y < 0 or snake_x > width or snake_y > height:
                game_over = True
                pygame.mixer.music.load(os.path.join(path, 'imgs\end_game.mp3'))
                pygame.mixer.music.play()
                time.sleep(2)
            gameWindow.fill(white)
            gameWindow.blit(gameBgImage, (0, 0))
            text_screen("Score : "+str(score)+"  High Score : "+str(highscore), black, 5, 5)
            head = []
            head.append(snake_x)
            head.append(snake_y)
            snk_lst.append(head)
            if len(snk_lst) > snk_length:
                del snk_lst[0]
            if head in snk_lst[:-1]:
                game_over = True
                pygame.mixer.music.load(os.path.join(path, 'imgs\end_game.mp3'))
                pygame.mixer.music.play()
                time.sleep(2)
            plot_snake(gameWindow, black, snk_lst, snake_size, head)
            # pygame.draw.rect(gameWindow, black, [snake_x, snake_y, snake_size, snake_size])
            pygame.draw.rect(gameWindow, red, [food_x, food_y, snake_size, snake_size])
        pygame.display.update()
        clock.tick(fps)
            
    # quit Game
    pygame.quit()
    quit()

if __name__ == "__main__":
    pygame.mixer.music.load(os.path.join(path, 'imgs\game.mp3'))
    pygame.mixer.music.play(-1)
    welcome()